#!/usr/bin/php
<?php
require __DIR__."/vendor/autoload.php";
require __DIR__."/config.php";

use Dom\HTMLDocument;
use WebSocket\Client;

const BASE_CONFIGURATION=array(
	"time_point_1"=>"00:00",
	"time_point_2"=>"00:00",
	"time_point_3"=>"00:00",
	"time_point_4"=>"00:00",
	"time_point_5"=>"00:00",
	"time_point_6"=>"00:00"
);

const OPENWEATHERMAP_FORECAST_ENDPOINT="/data/2.5/forecast";

if(	!defined("DEBUG_MODE")||!is_bool(DEBUG_MODE)||
	!defined("LOG_LEVEL")||!is_int(LOG_LEVEL)||
	!defined("SOLAR_ASSISTANT_HOSTNAME")||
	!defined("TIMESTAMP_OFFSET")||!is_int(TIMESTAMP_OFFSET)||
	!defined("WEBSOCKET_TIMEOUT")||!is_int(WEBSOCKET_TIMEOUT)||WEBSOCKET_TIMEOUT<1||
	!defined("RETRY_INTERVAL")||!is_int(RETRY_INTERVAL)||RETRY_INTERVAL<1||
	!defined("CONFIRM_INTERVAL")||!is_int(CONFIRM_INTERVAL)||CONFIRM_INTERVAL<1||
	!defined("SCHEDULE_FILE")
){
	throw new ConfigurationException("Missing or invalid configuration; ALL the values in config.php MUST be defined and not malformed");
}

pcntl_signal(SIGHUP,function($signal){
	logWrite(LOG_INFO,"SIGHUP received, reloading schedule…");
	loadScheduleFile();
	return;
});

$interactiveShell=posix_isatty(STDOUT);
if(!$interactiveShell){
	openlog(basename($_SERVER["PHP_SELF"]),0,LOG_DAEMON);
}

$scheduleFile=null;
$scheduleFileJustLoaded=false;
loadScheduleFile();

while(true){
	try{
		$GLOBALS["scheduleFileJustLoaded"]=false;
		
		$currentDateTime=new DateTimeImmutable();
		$currentTimestamp=$currentDateTime->getTimestamp();
		
		$yearlySchedule=generateSchedule($currentDateTime,$scheduleFile["yearlySchedule"]);
		
		$currentDailySchedule=null;
		$nextYearlyScheduleStartTimestamp=null;
		foreach($yearlySchedule as $startTimestamp=>$dailySchedule){
			if($currentTimestamp<$startTimestamp){
				$nextYearlyScheduleStartTimestamp=$startTimestamp;
				break;
			}
			$currentDailySchedule=$dailySchedule;
		}
		
		$currentScheduleStartTimestamp=null;
		$currentScheduleProfileName=null;
		$nextDailyScheduleStartTimestamp=null;
		foreach($currentDailySchedule as $startTimestamp=>$scheduleProfileName){
			if($currentTimestamp<$startTimestamp){
				$nextDailyScheduleStartTimestamp=$startTimestamp;
				break;
			}
			$currentScheduleStartTimestamp=$startTimestamp;
			$currentScheduleProfileName=$scheduleProfileName;
		}
		
		$nextScheduleStartTimestamp=min($nextDailyScheduleStartTimestamp,$nextYearlyScheduleStartTimestamp);
		
		$schedule=$scheduleFile["scheduleProfiles"][$currentScheduleProfileName];
		logWrite(LOG_NOTICE,"Applying schedule {}",json_encode($schedule));
		
		$autoSell=array_key_exists("autoSell",$schedule)&&$schedule["autoSell"];
		$autoSellDone=false;
		$backupMode=file_exists(BACKUP_MODE_FILE);
		$sellInhibit=file_exists(BATTERY_SELL_INHIBIT_FILE);
		
		if($autoSell){
			logWrite(LOG_INFO,"This is an autoSell period, initially applying no-sell settings");
			applyInverterConfiguration(scheduleToInverterConfiguration($schedule,$backupMode,true),$nextScheduleStartTimestamp,false);
			
			if($sellInhibit||$backupMode){
				logWrite(LOG_INFO,"This is an autoSell period but backup mode or battery sell inhibit mode is enabled, not selling");
			}else if(time()-300>$currentScheduleStartTimestamp){
				logWrite(LOG_INFO,"This is an autoSell period but the current time is more than 5 minutes after the schedule period began, not selling");
			}else{
				try{
					$averages=getAverageTemperatureAndCloudCover($schedule["temperaturePoints"],$schedule["cloudCoverPoints"]);
					
					$temperatureMultiplier=getMultiplier($scheduleFile["temperatureMultiplierProfiles"][$schedule["temperatureMultiplierProfile"]],$averages["temperature"]);
					logWrite(LOG_DEBUG,"Calculated temperatureMultiplier={} from averageTemperature {}K/{}℉",$temperatureMultiplier,$averages["temperature"],kelvinToFahrenheit($averages["temperature"]));
					
					$cloudCoverMultiplier=getMultiplier($scheduleFile["cloudCoverMultiplierProfiles"][$schedule["cloudCoverMultiplierProfile"]],$averages["cloudCover"]);
					logWrite(LOG_DEBUG,"Calculated cloudCoverMultiplier={} from averageCloudCover {}%",$cloudCoverMultiplier,$averages["cloudCover"]);
					
					$batterySellTime=round(($nextScheduleStartTimestamp-time())*$temperatureMultiplier*$cloudCoverMultiplier);
					logWrite(LOG_INFO,"Calculated battery sell time is {}",secondsToTime($batterySellTime));
					if($batterySellTime>$schedule["batteryToGridCutoffSeconds"]){
						$batterySellEndTimestamp=time()+$batterySellTime;
						if($nextScheduleStartTimestamp<$batterySellEndTimestamp){
							$batterySellEndTimestamp=$nextScheduleStartTimestamp;
						}
						
						applyInverterConfiguration(scheduleToInverterConfiguration($schedule,false,false),$batterySellEndTimestamp,true);
						sleepUntilTimestamp($batterySellEndTimestamp);
						$autoSellDone=true;
					}else{
						logWrite(LOG_INFO,"Calculated battery sell time is less than configured cutoff {}, not selling",secondsToTime($schedule["batteryToGridCutoffSeconds"]));
					}
				}catch(OpenWeatherMapClientException $e){
					logWrite(LOG_WARN,"Error calling OpenWeatherMap API: {}, not selling",$e->getMessage());
				}
			}
		}
		
		if((!$autoSell||($autoSell&&$autoSellDone)||$sellInhibit)&&time()<$nextScheduleStartTimestamp){
			applyInverterConfiguration(scheduleToInverterConfiguration($schedule,$backupMode,$autoSell||$sellInhibit),$nextScheduleStartTimestamp,true);
		}
		
		sleepUntilTimestamp($nextScheduleStartTimestamp);
	}catch(ApplyInverterConfigurationException $e){
		logWrite(LOG_ERR,"Failed to apply inverter configuration: {} {}",$e->getMessage(),$e->getTraceAsString());
	}catch(ScheduleReloadedException $e){
		logWrite(LOG_NOTICE,"Schedule file reloaded");
	}catch(Exception $e){
		logWrite(LOG_ERR,"Unexpected exception: {} {}",$e->getMessage(),$e->getTraceAsString());
	}
}

function generateSchedule($currentDateTime,$rawYearlySchedule){
	$yearlySchedule=array();
	$currentYear=$currentDateTime->format("Y");
	foreach($rawYearlySchedule as $startDateTimeString=>$dailyScheduleName){
		$startDateTime=DateTimeImmutable::createFromFormat("Y-m-d\TH:i:s","{$currentYear}-{$startDateTimeString}");
		$dailySchedule=generateDailySchedule($currentDateTime,$GLOBALS["scheduleFile"]["dailySchedules"][$dailyScheduleName]);
		for($i=-1;$i<=1;$i++){
			$yearlySchedule[$startDateTime->modify("$i year")->getTimestamp()+TIMESTAMP_OFFSET]=$dailySchedule;
		}
	}
	
	ksort($yearlySchedule,SORT_NUMERIC);
	
	return $yearlySchedule;
}

function generateDailySchedule($currentDateTime,$rawDailySchedule){
	$dailySchedule=array();
	$currentYear=$currentDateTime->format("Y");
	$currentMonth=$currentDateTime->format("m");
	$currentDay=$currentDateTime->format("d");
	foreach($rawDailySchedule as $startTimeString=>$scheduleProfileName){
		$startTime=DateTimeImmutable::createFromFormat("Y-m-d\TH:i:s","{$currentYear}-{$currentMonth}-{$currentDay}T{$startTimeString}");
		for($i=-1;$i<=1;$i++){
			$dailySchedule[$startTime->modify("$i day")->getTimestamp()+TIMESTAMP_OFFSET]=$scheduleProfileName;
		}
	}
	
	ksort($dailySchedule,SORT_NUMERIC);
	
	return $dailySchedule;
}

function scheduleToInverterConfiguration($scheduleSettings,$backupMode,$inhibitBatterySell){
	$inverterConfiguration=array();
	$batteryToGrid=$scheduleSettings["batteryToGrid"];
	if($batteryToGrid&&($backupMode||$inhibitBatterySell)){
		logWrite(LOG_INFO,"Schedule configured for battery sell but battery sell disabled for autoSell or by backup or battery sell inhibit modes");
		$batteryToGrid=false;
	}
	if($batteryToGrid){
		$inverterConfiguration["power_point_6"]=$scheduleSettings["batteryToGridPower"];
	}else if(array_key_exists("maximumUsePower",$scheduleSettings)){
		$inverterConfiguration["power_point_6"]=$scheduleSettings["maximumUsePower"];
	}
	if($backupMode&&array_key_exists("backupModeMinimumStateOfCharge",$scheduleSettings)){
		$inverterConfiguration["capacity_point_6"]=$scheduleSettings["backupModeMinimumStateOfCharge"];
	}else if(array_key_exists("minimumStateOfCharge",$scheduleSettings)){
		$inverterConfiguration["capacity_point_6"]=$scheduleSettings["minimumStateOfCharge"];
	}
	if(array_key_exists("gridToBattery",$scheduleSettings)){
		$inverterConfiguration["grid_charge_point_6"]=$scheduleSettings["gridToBattery"]?1:0;
	}
	$inverterConfiguration["gen_charge_point_6"]=$batteryToGrid?1:0;
	return $inverterConfiguration;
}

function applyInverterConfiguration($inverterConfiguration,$terminateTimestamp,$confirm){
	while(time()<$terminateTimestamp){
		try{
			$inverterConfiguration=array_merge(BASE_CONFIGURATION,$inverterConfiguration);
			logWrite(LOG_NOTICE,"Requested inverter configuration: [{}]",formatInverterConfiguration($inverterConfiguration));
			
			$solarAssistantKey=solarAssistantLogin();
			$powerManagementPage=getSolarAssistantPowerManagementPage($solarAssistantKey);
			
			if(!diffInverterConfiguration($inverterConfiguration,$powerManagementPage)){
				logWrite(LOG_INFO,"Requested configuration is already active");
				return;
			}
			
			$csrfToken=null;
			$lv=null;
			$session=null;
			$static=null;
			
			$csrfToken=getElementsByTagNameAndAttribute($powerManagementPage,"meta","name","csrf-token")[0]->getAttribute("content");
			
			$phxMainDiv=getElementsByTagNameAndAttribute($powerManagementPage,"div","data-phx-main","")[0];
			$lv=$phxMainDiv->getAttribute("id");
			$session=$phxMainDiv->getAttribute("data-phx-session");
			$static=$phxMainDiv->getAttribute("data-phx-static");
			
			if($csrfToken===null||$lv===null||$session===null||$static===null){
				throw new ApplyInverterConfigurationFailureException("Failed to scrape Phoenix IDs or CSRF token from power_management page");
			}
			logWrite(LOG_DEBUG,"Retrieved Phoenix IDs");
			
			if(!DEBUG_MODE){
				pushInverterConfiguration($inverterConfiguration,$solarAssistantKey,$csrfToken,$lv,$session,$static);
				
				if($confirm&&time()+CONFIRM_INTERVAL>=$terminateTimestamp){
					logWrite(LOG_INFO,"Confirmation delay time is after timeout, skipping confirmation");
				}else if($confirm){
					reloadScheduleIfNeeded();
					logWrite(LOG_INFO,"Waiting {} before confirming values…",secondsToTime(CONFIRM_INTERVAL));
					sleep(CONFIRM_INTERVAL);
					reloadScheduleIfNeeded();
					logWrite(LOG_INFO,"Confirming values…");
					if(diffInverterConfiguration($inverterConfiguration,getSolarAssistantPowerManagementPage($solarAssistantKey))){
						throw new ApplyInverterConfigurationFailureException("solar-assistant said values were applied but they were not");
					}
					logWrite(LOG_INFO,"Values confirmed");
				}
			}
			
			logWrite(LOG_NOTICE,"Done");
			return;
		}catch(Exception $e){
			logWrite(LOG_WARNING,"Error applying settings: {} {}, retrying in {} seconds",$e->getMessage(),$e->getTraceAsString(),RETRY_INTERVAL);
			reloadScheduleIfNeeded();
			sleep(RETRY_INTERVAL);
			reloadScheduleIfNeeded();
		}
	}
	throw new ApplyInverterConfigurationTimeoutException("Failed to apply inverter configuration before timeout");
}

function pushInverterConfiguration($inverterConfiguration,$solarAssistantKey,$csrfToken,$lv,$session,$static){
	$inverterConfigurationString=formatInverterConfiguration($inverterConfiguration);
	logWrite(LOG_INFO,"Pushing inverter configuration [{}]",$inverterConfigurationString);
	
	$joinMessage=[
		"4",
		"4",
		"lv:$lv",
		"phx_join",
		[
			"url"=>"http://".SOLAR_ASSISTANT_HOSTNAME."/inverter/power_management",
			"session"=>$session,
			"static"=>$static,
		]
	];
	
	$updateScheduleMessage=[
		"4",
		"34",
		"lv:$lv",
		"event",
		[
			"type"=>"form",
			"event"=>"save_settings",
			"value"=>$inverterConfigurationString,
			"cid"=>1
		]
	];
	
	$valuesToUpdate=array();
	foreach(array_keys($inverterConfiguration) as $scheduleSettingName){
		$scheduleFieldName=str_replace(array("grid_","gen_"),"",explode("=",$scheduleSettingName)[0]);
		$valuesToUpdate[$scheduleFieldName]="";
	}
	
	$client=new Client("ws://".SOLAR_ASSISTANT_HOSTNAME."/live/websocket?_csrf_token=$csrfToken&vsn=2.0.0");
	$client->addHeader("Cookie", "_solar_assistant_key=$solarAssistantKey");
	$client->setTimeout(WEBSOCKET_TIMEOUT);
	
	try{
		$client->text(json_encode($joinMessage));
		$joinResponseString=$client->receive()->getContent();
		$joinResponse=json_decode($joinResponseString);
		
		if($joinResponse===null){
			throw new ApplyInverterConfigurationFailureException("Failed to deserialize response [$joinResponseString] as JSON");
		}
		
		if(count($joinResponse)>=5&&$joinResponse[4]->status!=="error"){
			logWrite(LOG_INFO,"Successfully connected to Phoenix");
		}else{
			throw new ApplyInverterConfigurationFailureException("Failed to connect to Phoenix: ".var_export($joinResponse));
		}
		
		$client->text(json_encode($updateScheduleMessage));
		logWrite(LOG_INFO,"Submitted schedule data");
		
		$lastResponseTimestamp=time();
		while(true){
			$responseString=$client->receive()->getContent();
			$response=json_decode($responseString,true);
			
			if($response===null){
				throw new ApplyInverterConfigurationFailureException("Failed to deserialize response [$responseString] as JSON");
			}
			
			if(count($response)>=5&&$response[3]==="diff"&&is_array($response[4])){
				if(array_key_exists("e",$response[4])){
					foreach($response[4]["e"] as $result){
						if(count($result)===2&&$result[1]["class"]==="positive"){
							logWrite(LOG_DEBUG,"Successfully updated [{}]",$result[1]["id"]);
							unset($valuesToUpdate[$result[1]["id"]]);
							if(count($valuesToUpdate)===0){
								logWrite(LOG_INFO,"Successfully updated all values, waiting for values to apply…");
							}
						}
					}
					$lastResponseTimestamp=time();
				}else if(array_key_exists("c",$response[4])&&is_array($response[4]["c"])&&count($valuesToUpdate)===0){
					$changeArrayWrapper=$response[4]["c"];
					$scheduleApplied=false;
					foreach($changeArrayWrapper as $changeArray){
						foreach($changeArray as $changeString){
							$change=HTMLDocument::createFromString($changeString,LIBXML_NOERROR);
							if($change->getElementById("timer")!==null&&!diffInverterConfiguration($inverterConfiguration,$change)){
								$scheduleApplied=true;
								break;
							}
						}
						if($scheduleApplied===true){
							break;
						}
					}
					if($scheduleApplied===true){
						logWrite(LOG_INFO,"Values applied");
						break;
					}
				}
			}
			
			if($lastResponseTimestamp+WEBSOCKET_TIMEOUT<time()){
				throw new ApplyInverterConfigurationFailureException("More than ".WEBSOCKET_TIMEOUT." seconds elapsed without a useful response");
			}
		}
	}finally{
		$client->close();
	}
}

function formatInverterConfiguration($inverterConfiguration){
	$inverterConfigurationCondensed=array();
	foreach($inverterConfiguration as $settingName=>$settingValue){
		$inverterConfigurationCondensed[]=$settingName."=".urlencode($settingValue);
	}
	return implode("&",$inverterConfigurationCondensed);
}

function diffInverterConfiguration(&$inverterConfiguration,$htmlNode){
	foreach($inverterConfiguration as $setting=>$value){
		$existingValue=null;
		if(($existingValueInput=$htmlNode->getElementById("{$setting}_check"))!==null){
			$existingValue=(int)filter_var($existingValueInput->getAttribute("data-value"),FILTER_VALIDATE_BOOLEAN);
		}else if(($existingValueDiv=$htmlNode->getElementById($setting))!==null){
			$existingValue=preg_replace("/[^0-9:]/","",$existingValueDiv->textContent);
			if(preg_match("/^[0-9]*$/",$existingValue)){
				$existingValue=(int)$existingValue;
			}
		}else{
			throw new ApplyInverterConfigurationFailureException("Could not find div for setting [$setting]");
		}
		
		if($existingValue===$value){
			unset($inverterConfiguration[$setting]);
		}
	}
	return count($inverterConfiguration)>0;
}

function solarAssistantLogin(){
	$loginContext=stream_context_create([
		"http"=>[
			"follow_location"=>false
		]
	]);
	if(file_get_contents("http://".SOLAR_ASSISTANT_HOSTNAME."/sign_in",false,$loginContext)===false){
		throw new HTTPClientException("Failed to load solar-assistant sign_in page");
	}
	
	$solarAssistantKey=solarAssistantKeyFromHTTPHeaders($http_response_header);
	if($solarAssistantKey===null){
		throw new HTTPClientException("solar-assistant login page did not have expected authentication cookie");
	}
	
	logWrite(LOG_DEBUG,"Logged in to solar-assistant");
	return $solarAssistantKey;
}

function getSolarAssistantPowerManagementPage(&$solarAssistantKey){
	$powerManagementContext=stream_context_create([
		"http"=>[
			"header"=>"Cookie: _solar_assistant_key=$solarAssistantKey\r\n"
		]
	]);
	$powerManagementPageString=file_get_contents("http://".SOLAR_ASSISTANT_HOSTNAME."/inverter/power_management",false,$powerManagementContext);
	
	if($powerManagementPageString===false){
		throw new HTTPClientException("Failed to load solar-assistant power_management page");
	}

	$newSolarAssistantKey=solarAssistantKeyFromHTTPHeaders($http_response_header);
	if($newSolarAssistantKey!==null){
		$solarAssistantKey=$newSolarAssistantKey;
	}
	
	logWrite(LOG_DEBUG,"Loaded solar-assistant power_management page");
	return HTMLDocument::createFromString($powerManagementPageString,LIBXML_NOERROR);
}

function solarAssistantKeyFromHTTPHeaders($headers){
	foreach($headers as $header){
		$headerArray=explode(":",$header,2);
		if(count($headerArray)===2&&$headerArray[0]==="set-cookie"){
			$cookieArray=explode("=",explode(";",trim($headerArray[1]),2)[0],2);
			if(count($cookieArray)===2&&$cookieArray[0]==="_solar_assistant_key"){
				return $cookieArray[1];
			}
		}
	}
	return null;
}

function getAverageTemperatureAndCloudCover($temperaturePoints,$cloudCoverPoints){
	$temperatureArray=array();
	$cloudCoverArray=array();
	if(LOG_DEBUG<=LOG_LEVEL){
		$temperatureString="Temperature:";
		$cloudCoverString="Cloud Cover:";
	}
	$forecast=getOpenWeatherMapForecast(LOCATION,max($temperaturePoints,$cloudCoverPoints));
	foreach($forecast->list as $listItem){
		if(property_exists($listItem,"main")&&property_exists($listItem->main,"temp")&&count($temperatureArray)<$temperaturePoints){
			$temperatureArray[]=$listItem->main->temp;
			if(LOG_DEBUG<=LOG_LEVEL){
				$temperatureString.=" ".formatTimestamp($listItem->dt)."=>{$listItem->main->temp}K/".kelvinToFahrenheit($listItem->main->temp)."℉";
			}
		}
		if(property_exists($listItem,"clouds")&&property_exists($listItem->clouds,"all")&&count($cloudCoverArray)<$cloudCoverPoints){
			$cloudCoverArray[]=$listItem->clouds->all;
			if(LOG_DEBUG<=LOG_LEVEL){
				$cloudCoverString.=" ".formatTimestamp($listItem->dt)."=>{$listItem->clouds->all}%";
			}
		}
	}
	if(LOG_DEBUG<=LOG_LEVEL){
		logWrite(LOG_DEBUG,$temperatureString);
		logWrite(LOG_DEBUG,$cloudCoverString);
	}
	$averages=array();
	$averages["temperature"]=arrayAverage($temperatureArray);
	$averages["cloudCover"]=arrayAverage($cloudCoverArray);
	return $averages;
}

function getOpenWeatherMapForecast($location,$periodCount){
	$queryString=http_build_query([
		"q"=>$location,
		"cnt"=>$periodCount,
		"appid"=>OPENWEATHERMAP_APIKEY
	]);
	$response=file_get_contents("https://".OPENWEATHERMAP_DOMAIN.OPENWEATHERMAP_FORECAST_ENDPOINT."?$queryString");
	$data=json_decode($response);
	if($data){
		return $data;
	}else{
		throw new OpenWeatherMapClientException("Failed to deserialize OpenWeatherMap API response [$response] as JSON");
	}
}

function getElementsByTagNameAndAttribute($htmlDocument,$tagName,$attributeName,$attributeValue){
	$response=array();
	foreach($htmlDocument->getElementsByTagName($tagName) as $htmlElement){
		if($htmlElement->getAttribute($attributeName)===$attributeValue){
			$response[]=$htmlElement;
		}
	}
	return $response;
}

function formatTimestamp($timestamp){
	return (new DateTimeImmutable())->setTimestamp($timestamp)->format(DateTimeInterface::ISO8601);
}

function secondsToTime($seconds) {
	return (new DateTimeImmutable("@0"))->diff(new DateTimeImmutable("@$seconds"))->format("%h hours, %i minutes, and %s seconds");
}

function arrayAverage($array){
	if(count($array)>0){
		return array_sum($array)/count($array);
	}else{
		return null;
	}
}

function kelvinToFahrenheit($kelvinTemperature){
	return (9/5)*($kelvinTemperature-273.15)+32;
}

function sleepUntilTimestamp($timestamp){
	reloadScheduleIfNeeded();
	$nextWakeupDelay=max($timestamp-time(),0);
	logWrite(LOG_INFO,"Sleeping {} until {}",secondsToTime($nextWakeupDelay),formatTimestamp($timestamp));
	sleep($nextWakeupDelay);
	reloadScheduleIfNeeded();
}

function loadScheduleFile(){
	$scheduleFile=json_decode(file_get_contents(SCHEDULE_FILE),true);
	try{
		validateScheduleFile($scheduleFile);
		$GLOBALS["scheduleFile"]=$scheduleFile;
		$GLOBALS["scheduleFileJustLoaded"]=true;
	}catch(Exception $e){
		if($GLOBALS["scheduleFile"]===null){
			throw $e;
		}else{
			logWrite(LOG_ERR,"Error loading schedule; retaining previous schedule: {}: {}",$e->getMessage(),($e->getPrevious()!==null?($e->getPrevious()->getMessage()):"null"));
		}
	}
}

function reloadScheduleIfNeeded(){
	pcntl_signal_dispatch();
	if($GLOBALS["scheduleFileJustLoaded"]){
		throw new ScheduleReloadedException();
	}
}

function validateScheduleFile($scheduleFile){
	if($scheduleFile===false||$scheduleFile===null){
		throw new InvalidScheduleException("Failed to parse JSON in schedule file");
	}
	
	if(array_key_exists("temperatureMultiplierProfiles",$scheduleFile)){
		foreach($scheduleFile["temperatureMultiplierProfiles"] as $temperatureMultiplierProfileName=>&$temperatureMultipliers){
			if(count($temperatureMultipliers)===0){
				throw new InvalidScheduleException("temperatureMultiplierProfile [$temperatureMultiplierProfileName] must have at least one multiplier defined");
			}
			ksort($temperatureMultipliers,SORT_NUMERIC);
			foreach($temperatureMultipliers as $temperature=>$multiplier){
				try{
					validateNumeric($temperature,0,null);
				}catch(ValidationException $e){
					throw new InvalidScheduleException("temperatureMultiplier temperature in profile [$temperatureMultiplierProfileName] failed validation",null,$e);
				}
				try{
					validateNumeric($multiplier,0,null);
				}catch(ValidationException $e){
					throw new InvalidScheduleException("temperatureMultiplier multiplier in profile [$temperatureMultiplierProfileName] failed validation",null,$e);
				}
			}
		}
	}else{
		throw new InvalidScheduleException("temperatureMultiplierProfiles block missing from schedule file");
	}
	
	if(array_key_exists("cloudCoverMultiplierProfiles",$scheduleFile)){
		foreach($scheduleFile["cloudCoverMultiplierProfiles"] as $cloudCoverMultiplierProfileName=>&$cloudCoverMultipliers){
			if(count($cloudCoverMultipliers)===0){
				throw new InvalidScheduleException("cloudCoverMultiplierProfile [$cloudCoverMultiplierProfileName] must have at least one multiplier defined");
			}
			ksort($cloudCoverMultipliers,SORT_NUMERIC);
			foreach($cloudCoverMultipliers as $cloudCover=>$multiplier){
				try{
					validateNumeric($cloudCover,0,100);
				}catch(ValidationException $e){
					throw new InvalidScheduleException("cloudCoverMultiplier cloudCover in profile [$cloudCoverMultiplierProfileName] failed validation",null,$e);
				}
				try{
					validateNumeric($multiplier,0,null);
				}catch(ValidationException $e){
					throw new InvalidScheduleException("cloudCoverMultiplier multiplier in profile [$cloudCoverMultiplierProfileName] failed validation",null,$e);
				}
			}
		}
	}else{
		throw new InvalidScheduleException("cloudCoverMultiplierProfiles block missing from schedule file");
	}
	
	if(array_key_exists("scheduleProfiles",$scheduleFile)){
		foreach($scheduleFile["scheduleProfiles"] as $scheduleProfileName=>$scheduleProfileSettings){
			if(is_array($scheduleProfileSettings)){
				foreach($scheduleProfileSettings as $scheduleProfileSettingName=>$scheduleProfileSettingValue){
					try{
						switch($scheduleProfileSettingName){
							case "maximumUsePower":
							case "batteryToGridPower":
							case "temperaturePoints":
							case "cloudCoverPoints":
							case "batteryToGridCutoffSeconds":
								validateInteger($scheduleProfileSettingValue,0,null);
								break;
							case "minimumStateOfCharge":
							case "backupModeMinimumStateOfCharge":
								validateInteger($scheduleProfileSettingValue,0,100);
								break;
							case "gridToBattery":
								validateBoolean($scheduleProfileSettingValue);
								break;
							case "autoSell":
								validateBoolean($scheduleProfileSettingValue);
								validateChildSettingExists($scheduleProfileSettingValue,"batteryToGridCutoffSeconds",$scheduleProfileSettings);
								validateChildSettingExists($scheduleProfileSettingValue,"temperatureMultiplierProfile",$scheduleProfileSettings);
								validateChildSettingExists($scheduleProfileSettingValue,"temperaturePoints",$scheduleProfileSettings);
								validateChildSettingExists($scheduleProfileSettingValue,"cloudCoverMultiplierProfile",$scheduleProfileSettings);
								validateChildSettingExists($scheduleProfileSettingValue,"cloudCoverPoints",$scheduleProfileSettings);
								break;
							case "batteryToGrid":
								validateBoolean($scheduleProfileSettingValue);
								validateChildSettingExists($scheduleProfileSettingValue,"batteryToGridPower",$scheduleProfileSettings);
								break;
							case "temperatureMultiplierProfile":
								if(!array_key_exists($scheduleProfileSettingValue,$scheduleFile["temperatureMultiplierProfiles"])){
									throw new InvalidScheduleException("Schedule profile [$scheduleProfileName] uses undefined temperatureMultiplierProfile [$scheduleProfileSettingValue]");
								}
								break;
							case "cloudCoverMultiplierProfile":
								if(!array_key_exists($scheduleProfileSettingValue,$scheduleFile["cloudCoverMultiplierProfiles"])){
									throw new InvalidScheduleException("Schedule profile [$scheduleProfileName] uses undefined cloudCoverMultiplierProfile [$scheduleProfileSettingValue]");
								}
								break;
							default:
								throw new InvalidScheduleException("Unrecognized setting [$scheduleProfileSettingName] in scheduleProfile [$scheduleProfileName]");
						}
					}catch(ValidationException $e){
						throw new InvalidScheduleException("Setting [$scheduleProfileSettingName] in scheduleProfile [$scheduleProfileName] failed validation",null,$e);
					}
				}
			}else{
				throw new InvalidScheduleException("Value for scheduleProfile [$scheduleProfileName] must be an array");
			}
		}
	}else{
		throw new InvalidScheduleException("scheduleProfiles block missing from schedule file");
	}
	
	if(array_key_exists("dailySchedules",$scheduleFile)){
		if(count($scheduleFile["dailySchedules"])<1){
			throw new InvalidScheduleException("There must be at least 1 daily schedule block");
		}
		foreach($scheduleFile["dailySchedules"] as $dailyScheduleName=>$dailySchedulePeriods){
			if(is_array($dailySchedulePeriods)){
				foreach($dailySchedulePeriods as $time=>$scheduleProfileName){
					if(DateTimeImmutable::createFromFormat("Y-m-d\TH:i:s","1970-01-01T$time")===false){
						throw new InvalidScheduleException("Daily schedule [$dailyScheduleName] has malformed time [$time]; correct format is H:i:s");
					}
					if(!array_key_exists($scheduleProfileName,$scheduleFile["scheduleProfiles"])){
						throw new InvalidScheduleException("Daily schedule [$dailyScheduleName] uses undefined scheduleProfile [$scheduleProfileName]");
					}
				}
			}else{
				throw new InvalidScheduleException("Value for dailySchedule [$dailyScheduleName] must be an array");
			}
		}
	}else{
		throw new InvalidScheduleException("dailySchedules block missing from schedule file");
	}
	
	if(array_key_exists("yearlySchedule",$scheduleFile)){
		if(count($scheduleFile["yearlySchedule"])<1){
			throw new InvalidScheduleException("There must be at least 1 yearly schedule block");
		}
		foreach($scheduleFile["yearlySchedule"] as $date=>$dailyScheduleName){
			if(DateTimeImmutable::createFromFormat("Y-m-d\TH:i:s","1970-$date")===false){
				throw new InvalidScheduleException("Yearly schedule [$date] is malformed; correct format is m-d\TH:i:s");
			}
			if(!array_key_exists($dailyScheduleName,$scheduleFile["dailySchedules"])){
				throw new InvalidScheduleException("Yearly schedule [$date] uses undefined dailySchedule [$dailyScheduleName]");
			}
		}
	}else{
		throw new InvalidScheduleException("yearlySchedule block missing from schedule file");
	}
}

function validateBoolean($value){
	if(!is_bool($value)){
		throw new ValidationException("Value [$value] must be a boolean");
	}
}

function validateInteger($value,$min,$max){
	if(!is_int($value)){
		throw new ValidationException("Value [$value] must be an integer");
	}
	validateNumericRange($value,$min,$max);
}

function validateNumeric($value,$min,$max){
	if(!is_numeric($value)){
		throw new ValidationException("Value [$value] must be a number");
	}
	validateNumericRange($value,$min,$max);
}

function validateNumericRange($value,$min,$max){
	if($min!==null&&$value<$min){
		throw new ValidationException("Value [$value] must be greater than or equal to [$min]");
	}
	if($max!==null&&$value>$max){
		throw new ValidationException("Value [$value] must be less than or equal to [$max]");
	}
}

function validateChildSettingExists($value,$childSetting,$settingArray){
	if($value&&!array_key_exists($childSetting,$settingArray)){
		throw new InvalidScheduleException("Child setting [$childSetting] must be defined but it is not");
	}
}

function getMultiplier($multiplierArray,$value){
	$multiplier=null;
	foreach($multiplierArray as $currentValue=>$currentMultiplier){
		if($currentValue>=$value){
			if($multiplier!==null){
				break;
			}else{
				return $currentMultiplier;
			}
		}
		$multiplier=$currentMultiplier;
	}
	return $multiplier;
}

function logWrite($priority,$message,...$substitutionValues){
	if($priority<=LOG_LEVEL){
		foreach($substitutionValues as $substitutionValue){
			if($substitutionValue instanceof \Closure){
				$substitutionValue=$substitutionValue();
			}
			$message=preg_replace("/{}/",$substitutionValue,$message,1);
		}
		if($GLOBALS["interactiveShell"]){
			fwrite($priority<=LOG_WARNING?STDERR:STDOUT,$message.PHP_EOL);
		}else{
			syslog($priority,$message);
		}
	}
}

abstract class ApplyInverterConfigurationException extends Exception{}
class ApplyInverterConfigurationFailureException extends ApplyInverterConfigurationException{}
class ApplyInverterConfigurationTimeoutException extends ApplyInverterConfigurationException{}
class ConfigurationException extends Exception{}
class HTTPClientException extends Exception{}
class InvalidScheduleException extends Exception{}
class OpenWeatherMapClientException extends Exception{}
class ScheduleReloadedException extends Exception{}
class ValidationException extends Exception{}
?>
